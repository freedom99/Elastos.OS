//=========================================================================
// Copyright (C) 2017 The Elastos Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//=========================================================================

module
{
    importlib("Org.Javia.Arity.eco");
    importlib("Elastos.Droid.Core.eco");

    merge("car/elastos/droid/calculator3/expression/calculatorExpressionEvaluator.car");
    merge("car/elastos/droid/calculator3/expression/calculatorExpressionBuilder.car");
    merge("car/elastos/droid/calculator3/expression/calculatorExpressionTokenizer.car");

    using interface Elastos.Droid.Content.IContext;
    using interface Elastos.Droid.Text.ISpannableStringBuilder;
    using interface Elastos.Droid.Text.IGetChars;
    using interface Elastos.Droid.Text.ISpannable;
    using interface Elastos.Droid.Text.IEditable;
    using interface Elastos.Droid.Text.IGraphicsOperations;
    using interface Elastos.Droid.Text.ISpanned;
    using interface Elastos.Core.IAppendable;
    using interface Elastos.Core.ICharSequence;

    namespace Elastos{
    namespace Droid {
    namespace Calculator3 {
    namespace Expression {

    class CCalculatorExpressionBuilder
    {
        constructor(
            [in] ICharSequence* text,
            [in] ICalculatorExpressionTokenizer* tokenizer,
            [in] Boolean isEdited);

        interface ICalculatorExpressionBuilder;
        interface ISpannableStringBuilder;
        interface ICharSequence;
        interface IGetChars;
        interface ISpannable;
        interface ISpanned;
        interface IEditable;
        interface IAppendable;
        interface IGraphicsOperations;
    }

    class CCalculatorExpressionEvaluator
    {
        constructor(
            [in] ICalculatorExpressionTokenizer* tokenizer);

        interface ICalculatorExpressionEvaluator;
    }

    [local]
    class CCalculatorExpressionTokenizer
    {
        constructor(
            [in] IContext* context);

        interface ICalculatorExpressionTokenizer;
    }

    } // namespace Expression
    } // namespace Calculator3
    } // namespace Droid
    } // namespace Elastos
}